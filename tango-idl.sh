#! /usr/bin/env bash

# Copyright 2022, Thomas Juerges
# SPDX-License-Identifier: Apache-2.0

# tango-idl build script for macOS.

ABSOLUTE_PATH=$(realpath $(dirname ${BASH_SOURCE}))
export MY_PATH=${1:-$(realpath ${ABSOLUTE_PATH})}
SHORT_NAME=$(basename ${BASH_SOURCE})
export MY_NAME=${SHORT_NAME%.sh}

# Run everything in sub-shells. Saves me from all the pushd/popd.
(
# Load the default settings.
. ${MY_PATH}/settingsrc.sh
begin_message

############
# MODIFY ME!
############
#
# Everything that has been set in buildrc.sh can be
# overwritten here.
#
# Customise the repository location.
export REPOSITORY=${TANGO_CONTROLS_REPO_HOST}/${MY_NAME}.git

# Customise the branch which will be checked out by setting TANGO_BRANCH_OR_TAG.
export BRANCH_OR_TAG=${TANGO_BRANCH_OR_TAG}

{ build_generic ${MY_NAME} ${REPOSITORY} ${BRANCH_OR_TAG}; } || exit
end_message
)
