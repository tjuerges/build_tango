# Build Tango Controls on macOS

## Boiler plate

### macOS

This repo is first and foremost meant to help build keyparts of Tango Controls on macOS. This is not an official Tango Controls blessed repository, but in Tango Controls we are trying to support macOS.

### It possibly works on Linux, too

I have used this repo to build the same keyparts of Tango Controls on my Aarch64 Gentoo and my Ubuntu 22.04 systems. You are welcome to try it on your system, but I will not provide any support for Linux-related issues. MRs for Linux are welcome as long as they do not add functionality solely for the Linux side of these build scripts.

:point_up: **Note**: To keep things simple I decided that on Linux `cppzmq` will always be installed from its repo.

## Requirements (executive summary)

- Install [Homebrew](https://brew.sh) if your are on macOS! On Linux you might be successful without it.

- If you are **not** on macOS Ventura or later, i.e. the macOS product version is smaller than 13.0.0, then you need to install Homebrew's `coreutils` package. It contains the `realpath`utility. I use the `realpath` utility in every build script to detect its own full path in the filesystem. If you are not sure on which macOS version you are, then run the command `sw_vers -productVersion` like this:

    ```Bash
    thomas@okeanos build_tango (master) 22: sw_vers -productVersion
    12.6.3
    ```

    You can see that one of my laptops still runs macOS 12 which is Monterey and not Ventura.

- A Python3 venv for `pytango`

    You need a Python3 venv. Period.

    Create a Python3 venv where you want `pytango` to be installed. This will avoid messing up your system. You must install `numpy`, because `numpy` is now a requirement when building `pytango`.

    ```shell
    export MY_ENV=~/tmp/tango-env
    python3 -m venv --upgrade-deps ${MY_ENV}
    . ${MY_ENV}/bin/activate
    python3 -m pip install numpy
    ```

Everything else either comes already with macOS or will be automatically installed by the `install_requirements.sh` script.

## Which tags or branches of the packages are built?

The tags or branches are configured in the `versions.sh` file. It specifies default versions for all packages that can be built by the scripts in this repository. If you would like to build a different version of a package than configured in `versions.sh`, perhaps because you want to add a new feature or debug something, then overwrite the version by prefixing the individual build script with it like this:

```bash
TANGO_BRANCH_OR_TAG=Starter-1.1 ./starter.sh
```

:point_up: **Note**: Do not attermpt to overwrite any version when running `./build.sh`. The scripts will not like it and exit with an error.

## The settings file `settingsrc.sh`

Modify the settings in `settingsrc.sh` that are between

```bash
 ############
 # MODIFY ME!
 ############
```

and

```bash
#
# Leave this alone.
#
```

### But I am lazy

If you are too lazy to modify the settings, please do yourself a favour and at least read what is set there. You will be surprised what is all set wrong for __your__ system. Support requests that are caused by your lazyness will simply be closed.

:point_up: **Note**: If you are really really happy with the defaults, then all packages will be installed in `~/.tango/tango.main`. You can overwrite the default installation path by exporting it:

```bash
export TANGO_INSTALLATION_DIR=Directory_where_to_install_Tango_Controls_on_your_system
```

This allows you to keep `settingsrc.sh ` unmodified and `git` will not bother you all the time.

## Overwriting default settings in `settingsrc.sh`

The `settingsrc.sh` file allows you to overwrite its defaults. It will then happily assign what you set to its internally used environment variables. This is the list of variables which you can set before running any of the scripts in this repository. The scripts will not overwrite these values but instead accept what you set.

- C_STD: Set this to the C-standard that you want the C-compiler to apply. It needs to be a value which your C-compiler will accept for the `-std=` parameter. The default is `c11`.
- CC: The C-compiler that will be used for compilation. This needs to be a full path unless the executable is in one of the directories in `${PATH}`. The default is `clang` on macOS and `gcc` on Linux.
- CXX: The C++-compiler that will be used for compilation. This needs to be a full path unless the executable is in one of the directories in `${PATH}`. The default is `clang++` on macOS and `g++` in Linux.
- CXX_STD: Set this to the C-standard that you want the C++-compiler to apply. It needs to be a value which your C++-compiler will accept for the `-std=` parameter. The default is `cxx14`.
- NCPU: Set this to the number of parallel compilation jobs. The default on macOS is read from `$(sysctl -n hw.logicalcpu)` and on Linux from `$(nproc)`.
- TANGO_ADDITIONAL_CMAKE_DEFINES: Add the provided string (in quotes!) to the `cmake` call. Example: `TANGO_ADDITIONAL_CMAKE_DEFINES="-DMAKE_VERBOSE_MAKEFILE=ON" ./cppTango.sh`. Default: empty string, i.e. nothing.
- TANGO_BRANCH_OR_TAG: This setting is useful when you want to just build one package from a custom branch or tag. When the individual build scripts run, but not the build-it-all script `build.sh`, they source the file `versions.sh`. This file contains the default tags or branches for all the packages that can be built. Setting variable allows you to specify a branch or tag which will overwrite the default from `versions.sh`. Example: `TANGO_BRANCH_OR_TAG=6.0.0 tango-idl.sh` will build `tango-idl` from the branch or tag `6.0.0`.
- TANGO_BUILD: Set this to a name for the top level build directory that gets created inside the repo directory. It is also used in TANGO_INSTALLATION_DIR. The default will be the `cppTango_DEFAULT_VERSION` that is read from the `versions.sh` file unless TANGO_VERSION is also set in the `versions.sh` file. See TANGO_VERSION!
- TANGO_BUILD_TESTS: Set this to 1 if you do want tests to be built. The default is 0.
- TANGO_CMAKE_BE_VERBOSE: Tell `cmake` to be verbose in its output. This will simply add `--verbose` to the `cmake` call. Default: `0`, i.e. `cmake` is non-verbose.
- TANGO_CONTROLS_REPO_HOST: Set this to the git repo host if your repo is not hosted by the official Tango Controls repos. This comes handy if you are a contributor but cannot push new branches to one of the official Tango Controls repo. See also `TANGO_BRANCH_OR_TAG`.
- TANGO_CFLAGS: Add additional C compiler flags to the ones already defined by the build scripts. Note: The flags defined by `TANGO_CFLAGS` are add *AFTER* the already existing `CFLAGS`. The default is an empty string `""`.
- TANGO_CXXFLAGS: Add additional C++ compiler flags to the ones already defined by the build scripts. Note: The flags defined by `TANGO_CXXFLAGS` are add *AFTER* the already existing `CXXFLAGS`. The default is an empty string `""`.
- TANGO_DEBUG_OR_RELEASE: Build the packages with or without debug symbols and also set the compiler's optimisation flags accordingly. The default is `Release`
- TANGO_HOST: Set this to your TANGO_HOST. The default is `$(hostname -f):10000`.
- TANGO_INSTALLATION_DIR: Everything that is built by the scripts will be installed there. If the directory does not exist, it will be created. The default is `~/.local/tango/tango.main`.
- TANGO_NO_BUILD: Set this to 1 if you do not want to build packages. Handy if you just want to run tests. The default is 0.
- TANGO_NO_INSTALL: Set this to 1 if you do not want to install packages. Handy if you just want to check if everything compiles. Be aware that if you set this for the `./build.sh` script and have already an older installation, there might be odd effects during compilation. It is best to use this step by step only. You have been warned! The default is 0.
- TANGO_OMNIORB_INSTALLATION_DIR: Set this to the installation directory where either omniORB is already installed or where, when built and installed, it should be installed. The default is `${TANGO_INSTALLTION_DIR}/../omniORB.${omniORB_DEFAULT_VERSION}` with `omniORB_DEFAULT_VERSION` as specified in `versions.sh`.
- TANGO_PYTANGO_BOOST_PYTHON_LIB: Set this to the name of the boost_python library. The default is `boost_python311`.
- TANGO_PYTANGO_DEBUG_OR_RELEASE: Build PyTango with or without debug information. The default is `Release` which will build `PyTango` without debug symbols.
- TANGO_REPLACE_TANGODB: Set this to 1 and an existing TangoDB will be replaced with a clean TangoDB. The default is 0. **Note:** For this to work the TangoDatabase's `create_db.sh` script is needed the the respective `tango_admin` user needs to have admin permissions in your RDBMS.
- TANGO_REPO_LOCAL_DIR: When set, it is expected that this points to the local directory of the repository. The default is unset, which tells the build scripts to `git clone--depth 1`  the repo. Setting it to an existing directory will instruct the build scripts to rsync a copy of the repo from the specified directory to the build directory. Sensible defaults are assumed where the remote repository might be located. At first this might look as an odd configuration variable but setting this to a local directory is very useful when you want to try out a patch before pushing it to a central remote repo. :point_up: **Note**: When set and the directory exists, either the branch or tag set in `versions.sh` will be checked out (default) or the one specified by `TANGO_BRANCH_OR_TAG` (overwrites the default set in `versions.sh`s).
- TANGO_RUN_TESTS: Setting this to 1 tells the scripts to run tests. If enabled, this can potentially fail bacause a package might not have any tests. The default is 0.
- TANGO_UPLOAD_PYTANGO_WHEEL: If this is set to `1`, then the `pytango_dist.sh` script will upload the PyTango wheel in the `dist` directory to your registry. I set this to `1` when I want to publish a newly built PyTango wheel to the `build_tango` registry.
- TANGO_VERSION:  Set this to a version that will used to generate `TANGO_BUILD`. The default is read set to the value of ${TANGO_VERSION_DEFAULT} in the `versions.sh` file.

## Automatically installed packages

The following packages will be installed with `brew` when you run `build.sh` or `install_requirements.txt`:

- `boost-python3`
- `catch2`
- `cmake`
- `cppzmq` (compiled from source)
- `git`
- `jpeg-turbo`
- `libsodium`
- `opentelemtry-cpp`
- `[omniorb]` (Either compiled from source or pulled from brew, your choice!)
- `pkg-config`
- `zeromq`

If you installed one or more of these already by other means, then you can modify this list in `settingsrc.sh`. Look for `CHECK_AND_INSTALL_PACKAGES`.

### mariadb package for TangoDatabase

If you want to build TangoDatabase, then you need MariasDB's `mysql` header files and libraries. If the `brew`-package `mariadb` is not installed, then the `TangoDatabase.sh` build script will install it before the DatabaseDS is built.

## Build it!

Now just run

```bash
./build.sh
```

After a short while the build process should be done and you will have `cppTango`, `TangoTest`, `UniversalTest`,  `tango_admin`, `TangoDatabase`, i.e. `Databaseds`, `starter` and `pytango` installed.

## Bits and pieces after the build

### Surprise! There's a PyTango wheel for you

Once the PyTango build is successfully done, the `pytango_dist.sh` script will have created a PyTango wheel for you. The wheel will be stored in the `dist` directory and contains all necessary Tango Controls, omniORB and other run-time libraries needed to use and run PyTango.

### TangoDatabase users

If you intend to install and run the `DatabaseDS` on macOS, then you need a Python3 environment, too. [MAX IV's incredible useful TangoDB tool `dsconfig`](https://gitlab.com/MaxIV/lib-maxiv-dsconfig.git) will come very handy. You do not need to build it from scratch but can simply install it with pip. Did I already mention that you really want to run everything in its own Python environment?

```shell
python3 -m pip install dsconfig
```

### Set some environment variables

Now you should export at least one environment variable if you want to explore the full Tango Controls world:

- `TANGO_HOST`: (Required) This is essential. Without a `TANGO_HOST`you won't be able to enjoy much of Tango Controls. The `TANGO_HOST` runs the TangoDB, which is something like Tango Control's phone book and registry in one place. Either you run the TangoDB in a container and expse the container's port 10000 or you run TangoDB elsewhere yourself. In both cases you `export TANGO_HOST=HOSTNAME:PORT`:

    ```bash
    export TANGO_HOST=foo.bar.universe:10000
    ```

- `PATH`: (Optional) You can add `${TANGO_INSTALLATION_DIR}/bin` to the path. This will make `TangoTest` and `Databaseds` availble without having to type in the full path to it every time you want to run it.

I have a small `alias` that takes care of all of this for me:

```bash
alias tcenv='export PATH=${PATH}:~/.local/tango/tango.main/bin; export TANGO_HOST=$(hostname -f):10000; . ${MY_WORKSPACE}/tango-env/bin/activate'
```

Whenever I need to do some Tango Controls stuff, I open a terminal, execute `tcenv` and am all set.

## Try it out

Now has come the time to try it out. I will assume that you followed my earlier recommendations and have a venv set up and installed `ipython` there.

```python
thomas@okeanos tango 17: ipython
Python 3.10.6 (main, Aug 11 2022, 13:49:25) [Clang 13.1.6 (clang-1316.0.21.2.5)]
Type 'copyright', 'credits' or 'license' for more information
IPython 8.4.0 -- An enhanced Interactive Python. Type '?' for help.

In [1]: import tango

In [2]: print(tango.utils.info())
PyTango 9.3.5 (9, 3, 5, 'dev', 0)
PyTango compiled with:
    Python : 3.10.6
    Numpy  : 1.23.2
    Tango  : 9.4.0
    Boost  : 1.79.0

PyTango runtime is:
    Python : 3.10.6
    Numpy  : 1.23.2
    Tango  : 9.4.0

PyTango running on:
uname_result(system='Darwin', node='okeanos.###.###', release='21.6.0', version='Darwin Kernel Version 21.6.0: Wed Aug 10 14:25:27 PDT 2022; root:xnu-8020.141.5~2/RELEASE_X86_64', machine='x86_64')
```

## What else?

### Build images

I have added `make_image.sh` which builds docker images that contain what can be built with `build.sh`. Give it a try!

### Use the Force: iTango

Install `itango` from pip. It is great!

:point_up: **Note**: Make certain that you install it in the same virtual environment that you installed pytango in.

```bash
python3 -m pip --require-virtualenv install itango
```

Enjoy!

## When things go wrong...

Well, tough luck. Good bye!

Just kidding. :wink:

### cppTango compilation

#### omniorb acts up

So far I have had only one situation where things were really weird and `cppTango` did not compile. As it turned out that was due to an older `omniorb` installation. This was the error message:

```bash
-- Build files have been written to: /Users/thomas.juerges/workspace.thomas/tango/build_tango/tango.9.4/cppTango/build
[  0%] Generate tango.h, tangoSK.cpp and tangoDynSK.cpp from idl
[  0%] Building CXX object log4tango/src/CMakeFiles/log4tango_objects.dir/Appender.cpp.o
[  0%] Building CXX object log4tango/src/CMakeFiles/log4tango_objects.dir/AppenderAttachable.cpp.o
[  1%] Building CXX object log4tango/src/CMakeFiles/log4tango_objects.dir/LayoutAppender.cpp.o
[  1%] Building CXX object log4tango/src/CMakeFiles/log4tango_objects.dir/FileAppender.cpp.o
[  1%] Building CXX object log4tango/src/CMakeFiles/log4tango_objects.dir/RollingFileAppender.cpp.o
[  1%] Building CXX object log4tango/src/CMakeFiles/log4tango_objects.dir/OstreamAppender.cpp.o
[  1%] Building CXX object log4tango/src/CMakeFiles/log4tango_objects.dir/Layout.cpp.o
/bin/sh: /usr/local/bin/omniidl: /usr/bin/python: bad interpreter: No such file or directory
make[2]: *** [cppapi/server/idl/tango.h] Error 126
make[1]: *** [cppapi/server/idl/CMakeFiles/idl_source.dir/all] Error 2
make[1]: *** Waiting for unfinished jobs....
[  2%] Building CXX object log4tango/src/CMakeFiles/log4tango_objects.dir/PatternLayout.cpp.o
```

As you can see, in line 10 the build process bails out because of a `bad interpreter`. Freely translated, this means that the build process tried to run `/ur/bin/python`, but it could not be found. Well, naturally it cannot, because macOS Monterey (12.3.1) does not provide Python2 any more. But guess what? The error message is misleading. What really needs to be done is to reinstall `omniorg`:

```bash
brew reinstall omniorb
```

Once this has been done, the build should proceed smoothly until the end.

#### pyTango compilation fails because it cannot find libboost_pythonXXX

Yep. This one could be a recurring issue. I figured that the solution is as simple as making certain that `boost-python3` is insrtalled and that `TANGO_PYTANGO_BOOST_PYTHON_LIB` is set to the right value. For instance is my Linux system a bit behind of brew and I have to compile pyTango like this:

```bash
TANGO_PYTANGO_BOOST_PYTHON_LIB=boost_python310 ./pytango.sh
```



### Other issues?

Open an issue [here](https://gitlab.com/tjuerges/build_tango/-/issues). I will then try to help.

