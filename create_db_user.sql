# Copyright 2022, Thomas Juerges
# SPDX-License-Identifier: Apache-2.0

CREATE USER IF NOT EXISTS tango_admin@localhost;

GRANT Create ON *.* TO 'tango_admin'@'localhost';
GRANT Alter ON tango.* TO 'tango_admin'@'localhost';
GRANT Create ON tango.* TO 'tango_admin'@'localhost';

GRANT Create view ON tango.* TO 'tango_admin'@'localhost';
GRANT Delete ON tango.* TO 'tango_admin'@'localhost';
GRANT Delete history ON tango.* TO 'tango_admin'@'localhost';
GRANT Drop ON tango.* TO 'tango_admin'@'localhost';
GRANT Index ON tango.* TO 'tango_admin'@'localhost';
GRANT Insert ON tango.* TO 'tango_admin'@'localhost';
GRANT References ON tango.* TO 'tango_admin'@'localhost';
GRANT Select ON tango.* TO 'tango_admin'@'localhost';
GRANT Show view ON tango.* TO 'tango_admin'@'localhost';
GRANT Trigger ON tango.* TO 'tango_admin'@'localhost';
GRANT Update ON tango.* TO 'tango_admin'@'localhost';
GRANT Alter routine ON tango.* TO 'tango_admin'@'localhost';
GRANT Create routine ON tango.* TO 'tango_admin'@'localhost';
GRANT Create temporary tables ON tango.* TO 'tango_admin'@'localhost';
GRANT Execute ON tango.* TO 'tango_admin'@'localhost';
GRANT Lock tables ON tango.* TO 'tango_admin'@'localhost;



GRANT Alter ON tango_database_ci.* TO 'tango_admin'@'localhost';
GRANT Create ON tango_database_ci.* TO 'tango_admin'@'localhost';

GRANT Create view ON tango_database_ci.* TO 'tango_admin'@'localhost';
GRANT Delete ON tango_database_ci.* TO 'tango_admin'@'localhost';
GRANT Delete history ON tango_database_ci.* TO 'tango_admin'@'localhost';
GRANT Drop ON tango_database_ci.* TO 'tango_admin'@'localhost';
GRANT Index ON tango_database_ci.* TO 'tango_admin'@'localhost';
GRANT Insert ON tango_database_ci.* TO 'tango_admin'@'localhost';
GRANT References ON tango_database_ci.* TO 'tango_admin'@'localhost';
GRANT Select ON tango_database_ci.* TO 'tango_admin'@'localhost';
GRANT Show view ON tango_database_ci.* TO 'tango_admin'@'localhost';
GRANT Trigger ON tango_database_ci.* TO 'tango_admin'@'localhost';
GRANT Update ON tango_database_ci.* TO 'tango_admin'@'localhost';
GRANT Alter routine ON tango_database_ci.* TO 'tango_admin'@'localhost';
GRANT Create routine ON tango_database_ci.* TO 'tango_admin'@'localhost';
GRANT Create temporary tables ON tango_database_ci.* TO 'tango_admin'@'localhost';
GRANT Execute ON tango_database_ci.* TO 'tango_admin'@'localhost';
GRANT Lock tables ON tango_database_ci.* TO 'tango_admin'@'localhost;
