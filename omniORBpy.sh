#! /usr/bin/env bash

# Copyright 2022, Thomas Juerges
# SPDX-License-Identifier: Apache-2.0

# omniORBpy build script for Linux distros without the matching package.

ABSOLUTE_PATH=$(realpath $(dirname ${BASH_SOURCE}))
export MY_PATH=${1:-$(realpath ${ABSOLUTE_PATH})}
SHORT_NAME=$(basename ${BASH_SOURCE})
export MY_NAME=${SHORT_NAME%.sh}

# Run everything in sub-shells. Saves me from all the pushd/popd.
(
# Load the default settings.
. ${MY_PATH}/settingsrc.sh

begin_message

# If omniORB is not installed, stop right here because we need it.
[[ ! -f ${OMNIORB_INSTALLATION_DIR}/bin/catior ]] && { _p "It appears that omniORB ${OMNIORB_INSTALLATION_DIR} is not installed. Cannot continue without it."; return 0; }

# Install omniORBpy in its own directory. This allows to nuke the
# tango directory, but keep the omniORB and omniORBpy installations.
export OMNIORBPY_INSTALLATION_DIR=$(normpath ${TANGO_INSTALLATION_DIR}/../omniORBpy.${omniORBpy_DEFAULT_VERSION})
[[ ! -z ${TANGO_OMNIORBPY_INSTALLATION_DIR} ]] && export OMNIORBPY_INSTALLATION_DIR=${TANGO_OMNIORBPY_INSTALLATION_DIR}
_p ${OMNIORBPY_INSTALLATION_DIR}

# If omniORBpy is already installed, skip building and installing it.
[[ -d ${OMNIORBPY_INSTALLATION_DIR}/lib ]] && { _p "It appears that omniORBpy is already installed in ${OMNIORBPY_INSTALLATION_DIR}. Will skip this step. If you want to re-install omniORBpy, then please \"rm -rf ${OMNIORBPY_INSTALLATION_DIR}\"."; return 0; }

[[ ${OS} = Darwin && $(pkg-config --variable=prefix openssl | grep -c openssl) -eq 0 ]] && { _p "The OpenSSL package does not specify the prefix variable. If that variable is not set, omniORB will not compile libomnisslTK. But libomnisslTK is needed fpr TLS encryption. Exiting."; exit -1; }

create_top_level_build_dir || exit
[[ -d ${MY_NAME} ]] && rm -rf ${MY_NAME}
create_tool_build_dir || exit

OMNIORBPY_NAME=omniORBpy-${TANGO_BRANCH_OR_TAG}
OMNIORBPY_TARBALL=${OMNIORBPY_NAME}.tar.bz2

_p "Fetching and unpacking ${MY_NAME} ${TANGO_BRANCH_OR_TAG}..."
curl -L -o ${OMNIORBPY_TARBALL} https://sourceforge.net/projects/omniorb/files/omniORBpy/${OMNIORBPY_NAME}/${OMNIORBPY_TARBALL}/download || exit
tar -x --strip-components=1 -f ${OMNIORBPY_TARBALL} || exit

mkdir build || exit
cd build || exit

# Enable TLS and disable unused longdoubles.
CONFIGURATION="--with-omniorb=${OMNIORB_INSTALLATION_DIR} --with-openssl --disable-longdouble"
# When building on Gentoo or inside Docker, the --build parameter should be provided.
[[ -f /etc/gentoo-release || ${DOCKER_ENV} -eq 1 ]] && CONFIGURATION="${CONFIGURATION} --build=$(uname -m)-unknown-linux-gnu"

_p "Configuring ${MY_NAME} ${TANGO_BRANCH_OR_TAG} with parameters ${CONFIGURATION} and installation PATH ${OMNIORBPY_INSTALLATION_DIR}..."
../configure --prefix=${OMNIORBPY_INSTALLATION_DIR} ${CONFIGURATION} || exit

_p "Building ${MY_NAME} ${TANGO_BRANCH_OR_TAG}..."
make -j${NCPU} all || exit

_p "Installing ${MY_NAME} ${TANGO_BRANCH_OR_TAG}..."
make -j${NCPU} install || exit

end_message
)
