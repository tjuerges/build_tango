#! /usr/bin/env bash

# Copyright 2022, Thomas Juerges
# SPDX-License-Identifier: Apache-2.0

# Script to create a Python3 wheel and publish it.

ABSOLUTE_PATH=$(realpath $(dirname ${BASH_SOURCE}))
export MY_PATH=${1:-$(realpath ${ABSOLUTE_PATH})}
SHORT_NAME=$(basename ${BASH_SOURCE})
export MY_NAME=${SHORT_NAME%.sh}

# Run everything in sub-shells. Saves me from all the pushd/popd.
(
# Load the default settings.
. ${MY_PATH}/settingsrc.sh
begin_message

############
# MODIFY ME!
############
#
# Everything that has been set in buildrc.sh can be
# overwritten here.
#
# Customise the repository location.
export REPOSITORY=${TANGO_CONTROLS_REPO_HOST}/${MY_NAME}.git

# Customise the branch which will be checked out by setting TANGO_BRANCH_OR_TAG.
# If TANGO_PYTANGO_BRANCH_OR_TAG is set, it takes priority over
# TANGO_BRANCH_OR_TAG.
export BRANCH_OR_TAG=${TANGO_BRANCH_OR_TAG}
[[ ! -z ${TANGO_PYTANGO_BRANCH_OR_TAG} ]] && export BRANCH_OR_TAG=${TANGO_PYTANGO_BRANCH_OR_TAG}

# The name of the Boost-Python library.  The setup.py script is not
# yet able to figure this out on macOS/Darwin.
export BOOST_PYTHON_LIB=${TANGO_PYTANGO_BOOST_PYTHON_LIB}

# Use as many CPUs for the compilation as have been defined in NCPUs.
PARALLEL_BUILD="--parallel ${NCPU}"

# If TANGO_DEBUG_OR_RELEASE is set to 1, then build with debug symbols.
PYTANGO_DEBUG=""
[[ -z ${TANGO_DEBUG_OR_RELEASE} ]] || [[ ${TANGO_DEBUG_OR_RELEASE} = Debug ]] && export PYTANGO_DEBUG="--debug"

function build_wheel_from_egg()
{
    PYTANGO_EGG=$(find ${MY_PATH}/build/${TANGO_BUILD}/pytango -type f -name "pytango*${PYTHON_VERSION}*.egg")
    [[ -z ${PYTANGO_EGG} ]] && { _p "Please (re)build PyTango, because the PyTango egg file cannot be located."; exit -1; }
    wheel convert --dest-dir ${WHEEL_DIR} ${PYTANGO_EGG}
    [[ ${?} -eq 0 ]] && {
        if [ ${OS} = Darwin ]; then
            PLATFORM_TAG=macosx_${_TANGO_MACOS_VERSION}_0
        else
            PLATFORM_TAG=linux
        fi
        export PLATFORM_TAG+=_${ARCH}
        WHEEL=$(find ${WHEEL_DIR} -type f -name "pytango*${PYTHON_VERSION_NO_DOT}*.whl")
        export PYTANGO_WHEEL=${MY_PATH}/dist/$(wheel tags --remove --abi-tag none --platform-tag ${PLATFORM_TAG} ${WHEEL})
        return 0
    }
    return -1
}

function check_and_install()
{
    is_installed=$(python3 -m pip --require-virtualenv list | grep -c ${1})
    [[ ${is_installed} -eq 1 ]] && return 0
    _p "Installing the Python package ${1} in your virtual environment in ${VIRTUAL_ENV} because it is needed in the next step."
    python3 -m pip --require-virtualenv install ${1}
    return ${?}
}

function delocate_wheel()
{
    if [ ${OS} = Darwin ]; then
        # Only delocate the wheel on macOS
        check_and_install delocate
        [[ ${?} -eq 0 ]] && {
            _p "Copying all run-time libraries to the wheel. This can take a moment because a lot of files need to be checked."
            delocate-wheel -w dist ${2}
        }
    else
        # On Linux use auditwheel
        type patchelf &>/dev/null
        [[ ${?} -ne 0 ]] && {
            _p "This step requires the patchelf executable. Please install the package for your distribution!"
            return -1
        }
        check_and_install auditwheel
        auditwheel repair --wheel-dir dist --plat ${1} ${2}
    fi
    return ${?}
}

function publish_wheel()
{
    [[ ! -z ${TANGO_UPLOAD_PYTANGO_WHEEL} ]] && [[ ${TANGO_UPLOAD_PYTANGO_WHEEL} -eq 1 ]] && {
            check_and_install twine
            [[ ${?} -eq 0 ]] && python3 -m twine upload --repository gitlab ${1}
            return ${?}
        }
    return -1
}

    [[ -z ${VIRTUAL_ENV} ]] && return -1

    export WHEEL_DIR=${MY_PATH}/dist
    [[ ! -d ${WHEEL_DIR} ]] && mkdir ${WHEEL_DIR}

    export PYTHON_VERSION=$(python3 --version|cut -d' ' -f2|cut -d. -f1-2)
    export PYTHON_VERSION_NO_DOT=${PYTHON_VERSION/./}

    PYTANGO_WHEEL=""
    build_wheel_from_egg
    ret=${?}
    [[ ${ret} -eq 0 ]] && delocate_wheel ${PLATFORM_TAG} ${PYTANGO_WHEEL}
    ret=${?}
    [[ ${ret} -eq 0 ]] && { _p "This script created a PyTango wheel \"${PYTANGO_WHEEL}\" for you in ${WHEEL_DIR}. Enjoy!"; publish_wheel ${PYTANGO_WHEEL}; }

end_message
)
