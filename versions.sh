# Copyright 2022, Thomas Juerges
# SPDX-License-Identifier: Apache-2.0

# The preferred versions of the tools that the scripts build.

TANGO_DEFAULT_VERSION=main

alarm_handler_DEFAULT_VERSION=master
cppTango_DEFAULT_VERSION=main
cppzmq_DEFAULT_VERSION=v4.7.1
hdbpp_cm_DEFAULT_VERSION=use-TangoCMakeModules
hdbpp_es_DEFAULT_VERSION=use-TangoCMakeModules
libhdbpp_DEFAULT_VERSION=main
libhdbpp_mysql_DEFAULT_VERSION=16-cpptango-9-4-x-compatibility
libhdbpp_sqlite_DEFAULT_VERSION=1-enable-build-on-macos
libhdbpp_timescale_DEFAULT_VERSION=2.4.0
omniORB_DEFAULT_VERSION=4.3.2
omniORBpy_DEFAULT_VERSION=4.3.2
pytango_DEFAULT_VERSION=develop
starter_DEFAULT_VERSION=main
tango_admin_DEFAULT_VERSION=Release_1.24
tango_idl_DEFAULT_VERSION=6.0.2
TangoDatabase_DEFAULT_VERSION=main
TangoTest_DEFAULT_VERSION=main
UniversalTest_DEFAULT_VERSION=main
