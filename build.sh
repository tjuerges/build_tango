#! /usr/bin/env bash

# Copyright 2022, Thomas Juerges
# SPDX-License-Identifier: Apache-2.0

# Build script for Tango Controls on macOS.
#

# Make certain that the realpath command is available. On Linux, realpath is
# part of the base system. On macOS it is available only from version 13 on.
# On macOS < 13 one needs to install the coreutils package.
# Begin by checking if this is macOS.
function realpath_error()
{
    echo -e "\n\n\t***** ERROR! This system (macOS ${_TANGO_MACOS_VERSION}) is supposed to have the realpath command available, but it does not. One cause could be that your PATH environment variable is not properly set. Please verify it. The build scripts cannot continue, because the realpath command is essential for their functioning. This script will now exit."
    exit -1
}
[[ -z ${_TANGO_DO_NOT_CHECK_FOR_COREUTILS_} ]] && [[ ${OS} = Darwin ]] && {
    # OK, it is macOS. Can realpath be found in the PATH?
    type realpath &> /dev/null
    [[ ${?} -eq 0 ]] || {
        # No.
        # Then check the macOS version.
        # If it is 13 or if the coreutils package is already installed, then
        # there is something seriously wrong and I cannot fix it here.
        # If the version is 12 or lower and the coreutils package has not been
        # installed yet, then I just install it.
        _TANGO_MACOS_VERSION=$(sw_vers -productVersion | cut -d'.' -f1)
        if [ ${_TANGO_MACOS_VERSION} -le 12 ]; then
            brew list coreutils &> /dev/null
            [[ ${?} -eq 0 ]] && realpath_error
            brew install coreutils
        else
            realpath_error
        fi
    }
    # Tell all build scripts to not check for the existence of the realpath
    # tool again.
    export _TANGO_DO_NOT_CHECK_FOR_COREUTILS_=1
}

ABSOLUTE_PATH=$(realpath $(dirname ${BASH_SOURCE}))
export MY_PATH=${1:-$(realpath ${ABSOLUTE_PATH})}

# This checks if the overwriting of a branch or tag for the single package has
# accidentally been set for this script. This should not be done and is verified
# later.
[[ ! -z ${TANGO_BRANCH_OR_TAG} ]] && export TANGO_BRANCH_OR_TAG_WAS_NOT_UNSET=1

# Run everything in sub-shells. Saves me from all the pushd/popd.
time {
# Load the default settings.
. ${MY_PATH}/settingsrc.sh

# Verify that TANGO_BRANCH_OR_TAG was unset when this script was called. This
# is a sanity check to prevent building all packages from the same and thus very
# likely non-existing or wrong branch.
[[ ! -z ${TANGO_BRANCH_OR_TAG_WAS_NOT_UNSET} ]] && {
    _p "ERROR: Please do not set TANGO_BRANCH_OR_TAG and execute this script. This would build all packages from the same tag or branch. This cannot work because the packages do not share the same versioning schema."
    exit -1;
}

. ${MY_PATH}/install_requirements.sh
[[ ${OS} = Linux ]] &&
{
    # On Linux always check if omniORB 4.3.x needs to be installed from source.
    # But on Gentoo do NOT install omniORB from source because it already has
    # omniORB 4.3.x.
    [[ -f /etc/gentoo-release ]] || { time . ${MY_PATH}/omniORB.sh; time . ${MY_PATH}/omniORBpy.sh; }
    # On Linux always install cppzmq from source.
    time . ${MY_PATH}/cppzmq.sh;
}
time . ${MY_PATH}/tango-idl.sh
time . ${MY_PATH}/cppTango.sh
time . ${MY_PATH}/TangoDatabase.sh
time . ${MY_PATH}/ReplaceTangoDatabase.sh
time . ${MY_PATH}/tango_admin.sh
time . ${MY_PATH}/TangoTest.sh
time . ${MY_PATH}/UniversalTest.sh
time . ${MY_PATH}/libhdbpp.sh
time . ${MY_PATH}/libhdbpp-mysql.sh
time . ${MY_PATH}/libhdbpp-timescale.sh
time . ${MY_PATH}/libhdbpp-sqlite.sh
time . ${MY_PATH}/hdbpp-cm.sh
time . ${MY_PATH}/hdbpp-es.sh
time . ${MY_PATH}/alarm-handler.sh
time . ${MY_PATH}/starter.sh
time . ${MY_PATH}/pytango.sh
time . ${MY_PATH}/pytango_dist.sh
}
