#! /usr/bin/env bash

# Copyright 2022, Thomas Juerges
# SPDX-License-Identifier: Apache-2.0

# make_image build script for Tango Controls docker images

ABSOLUTE_PATH=$(realpath $(dirname ${BASH_SOURCE}))
export MY_PATH=$(realpath ${ABSOLUTE_PATH})
SHORT_NAME=$(basename ${BASH_SOURCE})
export MY_NAME=${SHORT_NAME%.sh}

# Run everything in sub-shells. Saves me from all the pushd/popd.
(
# Load the default settings.
. ${MY_PATH}/settingsrc.sh
begin_message

function help()
{
    _p "The following parameters can be used:
    -a \"[archs to build images for]\": The CPU architectures that the images will be built for. Note that the quotes are important and that the archs need to be separated by spaces. If you do not specify the -a parameter, then images will be built for the default architectures. Default: \"${archs[*]}\"
    -b [buildx builder name]: Set the buildx builder name that will be created. Default: ${buildx_builder}
    -d: Delete buildx builder that was created. Default: ${delete_builder} (0 means no)
    -i \"[images to build]\": The set of images to be built. Note that the quotes are important and that the images need to be separated by spaces. If you do not specify the -i parameter, then all images will be built. Default: \"${images_to_build[*]}\"
    -p: Push the built image to the registry after a successful build. Default: Do not push the image.
    -r [docker registry]: Set the docker registry that will be used to store intermediate buildx images. Default: ${docker_registry}
    -s [subdirectory]: Set the registriy's subdirectory. Default: ${registry_subdir}
    -v [version]: Set a version for the images that they will be tagged with. Default: ${version}
    -h: Print this help."
    exit 0
}

# Defaults
buildx_builder=builder-tango
declare -a archs=(linux/arm64 linux/amd64 linux/arm/v5)
delete_builder=0
declare -a images_to_build=(base build_tango tango-idl cppTango tango_admin TangoDatabase TangoTest UniversalTest hdbpp-cm hdbpp-es-mariadb hdbpp-es-timescale alarm-handler pytango itango)
docker_registry=nas.senmut.net:51473
registry_subdir=tango
version=$(date -u +'%F-%H.%M.%S')
push_image=0

# Needs to be here. Otherwise a parameter r will not be able to call this.
function check_registry()
{
    [[ "$(curl -I -k -s https://${docker_registry%%/*}/ | head -n 1 | cut -d ' ' -f 2)" == "200" ]] && return 0
    return 1
}

while getopts "a:b:dhi:pr:s:v:" opt; do
    case ${opt} in
        a)  archs=(${OPTARG})
            ;;
        b)  buildx_builder=${OPTARG}
            ;;
        d)  delete_builder=1
            ;;
        h)  help
            ;;
        i)  images_to_build=(${OPTARG})
            ;;
        p)  push_image=1
            ;;
        r)  docker_registry=${OPTARG}
            check_registry ${docker_registry}
            [[ ${?} -ne 0 ]] && { _p "The docker registry ${docker_registry} is unreachable. Please try setting a registry with the -r parameter that can be reached and try again."; exit -1; }
            ;;
        s)  registry_subdir=${OPTARG}
            ;;
        v)  version=${OPTARG}
            ;;
    esac
done

trap ' {  [[ ${delete_builder} -eq 1 ]] && delete_buildx_builder; exit 0; }; ' ABRT EXIT HUP INT TERM QUIT

[[ ${delete_builder} -eq 1 ]] && _p "Will delete buildx builder ${buildx_builder} when done."
_p "Images that will be built: ${images_to_build}"
_p "Using docker registry ${docker_registry} to store intermediate images."
_p "Using the tag ${version} to tag image versions."

function adjust_ncpu()
{
    number_of_archs=${#archs[*]}
    if [ ${number_of_archs} -gt 1 ]; then
        let OLD_NCPU=${NCPU}
        let NCPU=${OLD_NCPU}/${number_of_archs}
        let CMAKE_BUILD_PARALLEL_LEVEL=${NCPU}
        _p "Multiple archs are requested. Adjusting NCPU from ${OLD_NCPU} to ${NCPU} and setting CMAKE_BUILD_PARALLEL_LEVEL=${CMAKE_BUILD_PARALLEL_LEVEL}."
    else
        _p "Will use NCPU=${NCPU} for builds and set CMAKE_BUILD_PARALLEL_LEVEL=${CMAKE_BUILD_PARALLEL_LEVEL}."
    fi
}

function check_docker_buildx()
{
    docker buildx version 2>&1 >/dev/null
    [[ ${?} -eq 0 ]] && return 1
    return 0
}

function check_if_builder_exists()
{
    docker buildx inspect ${buildx_builder} 2>&1 >/dev/null
    [[ ${?} -eq 0 ]] && return 1
    return 0
}

function create_buildx_builder()
{
    check_if_builder_exists
    [[ ${?} -eq 0 ]] && { docker buildx create --use --name ${buildx_builder} --driver docker-container --platform ${archs}; }
}

function delete_buildx_builder()
{
    check_if_builder_exists
    [[ ${?} -eq 1 ]] && docker buildx rm --force ${buildx_builder}
}

function build_image()
{
    image_name=${1}
    v=$(echo ${2} | tr '[:upper:]' '[:lower:]')
    tag_image_name=$(echo ${image_name} | tr '[:upper:]' '[:lower:]')
    platforms=${archs[*]}
    platforms=${platforms// /,}
    _p "Building image ${tag_image_name}:${v} for archs ${platforms} and pushing it to the registry..."
    registry_tag=${docker_registry}/${registry_subdir}/${tag_image_name}:${v}
    local_tag=${tag_image_name}:${v}
    _p "docker buildx build --builder ${buildx_builder} --platform ${platforms} --progress plain -t ${registry_tag} -t ${local_tag} --load --build-arg NCPU=${NCPU} --build-arg CMAKE_BUILD_PARALLEL_LEVEL=${CMAKE_BUILD_PARALLEL_LEVEL} --build-arg REPOSITORY=${docker_registry}/${registry_subdir} --build-arg VERSION=${v} --build-arg TANGO_BRANCH_OR_TAG=${TANGO_BRANCH_OR_TAG} -f containers/Dockerfile.${image_name#*-} ."
    docker buildx build --builder ${buildx_builder} --platform ${platforms} --progress plain -t ${registry_tag} -t ${local_tag} --load --build-arg NCPU=${NCPU} --build-arg CMAKE_BUILD_PARALLEL_LEVEL=${CMAKE_BUILD_PARALLEL_LEVEL} --build-arg REPOSITORY=${docker_registry}/${registry_subdir} --build-arg VERSION=${v} --build-arg TANGO_BRANCH_OR_TAG=${TANGO_BRANCH_OR_TAG} -f containers/Dockerfile.${image_name#*-} .
    return ${?}
}

function push_built_image()
{
    image_name=${1}
    v=$(echo ${2} | tr '[:upper:]' '[:lower:]')
    tag_image_name=$(echo ${image_name} | tr '[:upper:]' '[:lower:]')
    _p "Pushing image ${tag_image_name}:${v} to the registry..."
    tag=${docker_registry}/${registry_subdir}/${tag_image_name}:${v}
    docker push ${tag}
    _p "Image pushed as ${docker_registry}/${tag_image_name}:${v}."
}


check_registry ${docker_registry}
[[ ${?} -ne 0 ]] && { _p "The docker registry ${docker_registry} is unreachable. Please set a registry with the -r parameter and try again."; exit -1; }

# docker buildx is needed. Exit if unavailable.
check_docker_buildx
[[ ${?} -ne 1 ]] && { _p "The building of images requires docker buildx. It appears that this system does not have buildx installed. Exiting because this script cannot continue."; exit -1; }

# Create a docker buildx builder
create_buildx_builder

# Adjust the number of CPUs per process to take into account multiple archs.
adjust_ncpu

for image in ${images_to_build[*]}; do
    img_name=tango_image-${image}
    build_image ${img_name} ${version}
    [[ ${?} -eq 0 ]] && [[ ${push_image} -eq 1 ]] && push_built_image ${img_name} ${version}
    _p "Image ${img_name} is done."
done

[[ ${delete_builder} -eq 1 ]] && delete_buildx_builder

end_message
)
