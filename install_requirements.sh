#! /usr/bin/env bash

# Copyright 2022, Thomas Juerges
# SPDX-License-Identifier: Apache-2.0

# Script for macOS that installs tools to build Tango Controls on macOS.

# Make certain that the realpath command is available. On Linux, realpath is
# part of the base system. On macOS it is available only from version 13 on.
# On macOS < 13 one needs to install the coreutils package.
# Begin by checking if this is macOS.
function realpath_error()
{
    echo -e "\n\n\t***** ERROR! This system (macOS ${_TANGO_MACOS_VERSION}) is supposed to have the realpath command available, but it does not. One cause could be that your PATH environment variable is not properly set. Please verify it. The build scripts cannot continue, because the realpath command is essential for their functioning. This script will now exit."
    exit -1
}

[[ -z ${_TANGO_DO_NOT_CHECK_FOR_COREUTILS_} ]] && [[ ${OS} = Darwin ]] && {
    # OK, it is macOS. Can realpath be found in the PATH?
    type realpath &> /dev/null
    [[ ${?} -eq 0 ]] || {
        # No.
        # Then check the macOS version.
        # If it is 13 or if the coreutils package is already installed, then
        # there is something seriously wrong and I cannot fix it here.
        # If the version is 12 or lower and the coreutils package has not been
        # installed yet, then I just install it.
        _TANGO_MACOS_VERSION=$(sw_vers -productVersion | cut -d'.' -f1)
        if [ ${_TANGO_MACOS_VERSION} -le 12 ]; then
            brew list coreutils &> /dev/null
            [[ ${?} -eq 0 ]] && realpath_error
            brew install coreutils
        else
            realpath_error
        fi
    }
    # Tell all build scripts to not check for the existence of the realpath
    # tool again.
    export _TANGO_DO_NOT_CHECK_FOR_COREUTILS_=1
}

ABSOLUTE_PATH=$(realpath $(dirname ${BASH_SOURCE}))
export MY_PATH=${1:-$(realpath ${ABSOLUTE_PATH})}

# Run everything in sub-shells. Saves me from all the pushd/popd.
(
# Load the default settings.
. ${MY_PATH}/settingsrc.sh
begin_message "Installing required packages..."

for package in ${CHECK_AND_INSTALL_PACKAGES}; do
    brew_check_install ${package}
done

end_message "Installing required packages done."
)
