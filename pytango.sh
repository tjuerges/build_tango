#! /usr/bin/env bash

# Copyright 2022, Thomas Juerges
# SPDX-License-Identifier: Apache-2.0

# pyTango build script for macOS.

ABSOLUTE_PATH=$(realpath $(dirname ${BASH_SOURCE}))
export MY_PATH=${1:-$(realpath ${ABSOLUTE_PATH})}
SHORT_NAME=$(basename ${BASH_SOURCE})
export MY_NAME=${SHORT_NAME%.sh}

# Run everything in sub-shells. Saves me from all the pushd/popd.
(
# Load the default settings.
. ${MY_PATH}/settingsrc.sh
begin_message

############
# MODIFY ME!
############
#
# Everything that has been set in buildrc.sh can be
# overwritten here.
#
# Customise the repository location.
export REPOSITORY=${TANGO_CONTROLS_REPO_HOST}/${MY_NAME}.git

# Customise the branch which will be checked out by setting TANGO_BRANCH_OR_TAG.
export BRANCH_OR_TAG=${TANGO_BRANCH_OR_TAG}

# The name of the Boost-Python library.  The setup.py script is not
# yet able to figure this out on macOS/Darwin.
export BOOST_PYTHON_LIB=${TANGO_PYTANGO_BOOST_PYTHON_LIB}

# If TANGO_DEBUG_OR_RELEASE is set to 1, then build with debug symbols.
PYTANGO_DEBUG=""
[[ -z ${TANGO_DEBUG_OR_RELEASE} ]] || [[ ${TANGO_DEBUG_OR_RELEASE} = Debug ]] && export PYTANGO_DEBUG="--debug"

function create_directories()
{
    create_top_level_build_dir || return
    rsync_or_clone pytango ${REPOSITORY} ${BRANCH_OR_TAG} ${TANGO_REPO_LOCAL_DIR} || return
}

function build_pytango()
{
    cd pytango
    [[ ${OS} = Darwin ]] && export LDFLAGS="-L${BREW_ROOT_DIR}/lib ${LDFLAGS}"

    # On Gentoo the detection of cppzmq in TANGO_INSTALLATION_DIR is broken.
    [[ ${OS} = Linux ]] && [[ -f /etc/gentoo-release || ${DOCKER_ENV} -eq 1 ]] && export CMAKE_OPTIONS="-Dcppzmq_INCLUDE_DIR=${TANGO_INSTALLATION_DIR}/include -Dcppzmq_INCLUDE_FILE=${TANGO_INSTALLATION_DIR}/include/zmq.hpp ${CMAKE_OPTIONS}"

    export CMAKE_GENERATOR="Ninja"
    export CMAKE_ARGS="${CMAKE_OPTIONS} ${DEFAULT_CMAKE_OPTIONS} ${TANGO_CMAKE_VERBOSE}"

    _p "I will be using the following build flags:\n\tPKG_CONFIG_PATH=${PKG_CONFIG_PATH}\n\tPKG_LIBRARY_PATH=${PKG_LIBRARY_PATH}\n\tCXXFLAGS=${CXXFLAGS}\n\tLDFLAGS=${LDFLAGS}\n\tDEFAULT_CMAKE_OPTIONS=${DEFAULT_CMAKE_OPTIONS}\n\tCMAKE_OPTIONS=${CMAKE_OPTIONS}\n\tCMAKE_GENERATOR=${CMAKE_GENERATOR}\n\tCMAKE_BUILD_PARALLEL_LEVEL=${CMAKE_BUILD_PARALLEL_LEVEL}\n\tCMAKE_ARGS=${CMAKE_ARGS}"
    if [[ -z ${TANGO_NO_BUILD} ]] || [[ ${TANGO_NO_BUILD} -ne 1 ]]; then
        CMAKE_ARGS="${CMAKE_ARGS}" CMAKE_GENERATOR="${CMAKE_GENERATOR}" CMAKE_BUILD_PARALLEL_LEVEL="${CMAKE_BUILD_PARALLEL_LEVEL}"
python3 -m pip --require-virtualenv install .
        ret=${?}
        if [ ${ret} -ne 0 ]; then
            exit ${ret}
        fi
    else
        _p "Skipping build because TANGO_NO_BUILD=${TANGO_NO_BUILD}."
    fi
    if [[ -z ${TANGO_RUN_TESTS} || ${TANGO_RUN_TESTS} -eq 0 ]]; then
        _p "Will skip running tests because TANGO_RUN_TESTS=${TANGO_RUN_TESTS}."
    else
        python3 setup.py test
        ret=${?}
        if [ ${ret} -ne 0 ]; then
            exit ${ret};
        fi
    fi
}

    create_directories
    build_pytango
end_message
)
