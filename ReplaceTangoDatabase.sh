#! /usr/bin/env bash

# Copyright 2022, Thomas Juerges
# SPDX-License-Identifier: Apache-2.0

# Script to replace a TangoDB.

ABSOLUTE_PATH=$(realpath $(dirname ${BASH_SOURCE}))
export MY_PATH=${1:-$(realpath ${ABSOLUTE_PATH})}
SHORT_NAME=$(basename ${BASH_SOURCE})
export MY_NAME=${SHORT_NAME%.sh}

# Run everything in sub-shells. Saves me from all the pushd/popd.
(
# Load the default settings.
. ${MY_PATH}/settingsrc.sh

# Yes, this file contains MariaDB credentials. You just make certain that the
# user which are listed here do not have excessive permissions.
mysql=mariadb
mysql_admin=tango_admin
mysql_admin_passwd=secret
mysql_host=localhost
db_name=tango

_p "Resetting the TangoDB..."
if [ ${TANGO_REPLACE_TANGODB} -eq 1 ]; then
    {
        # Only execute if the TangoDB can be recreated.
        if [ -f ${TANGO_INSTALLATION_DIR}/share/tango/db/create_db.sh ]; then
            cd ${TANGO_INSTALLATION_DIR}/share/tango/db;

            connect="${mysql} -u${mysql_admin} -p${mysql_admin_passwd} -h${mysql_host}"

            if $(echo quit | ${connect} >/dev/null 2>&1); then
                # There be dragons!
                # Just kill the entire TangoDB.
                TANGO_DB=tango
                [[ ${TANGO_BUILD_TESTS} -eq 1 ]] && TANGO_DB=tango_database_ci
                ${mysql} -u${mysql_admin} -p${mysql_admin_passwd} -h${mysql_host} -e "DROP DATABASE ${TANGO_DB};" > /dev/null
                # And now recreate it.
                . create_db.sh || exit
                _p "Resetting the TangoDB done."
            else
                _p "Could not connect to the MariaDB. Resetting the TangoDB failed!"
            fi
        else
            _p "The TangoDB cannot be reset because the script ${TANGO_INSTALLATION_DIR}/share/tango/db/create_db.sh from TangoDatabase is missing. Perhaps you need to build TangoDatabase?"
        fi;
    };
else
    _p "The TangoDB will not be reset which is probably what you intended. If you want to reset the TangoDB, then check the setting of the TANGO_REPLACE_TANGODB variable."
fi;
)
