#! /usr/bin/env bash

# Copyright 2022, Thomas Juerges
# SPDX-License-Identifier: Apache-2.0

# libhdbpp-timescale build script for macOS.

ABSOLUTE_PATH=$(realpath $(dirname ${BASH_SOURCE}))
export MY_PATH=${1:-$(realpath ${ABSOLUTE_PATH})}
SHORT_NAME=$(basename ${BASH_SOURCE})
export MY_NAME=${SHORT_NAME%.sh}

# Run everything in sub-shells. Saves me from all the pushd/popd.
(
# Load the default settings.
. ${MY_PATH}/settingsrc.sh
begin_message

############
# MODIFY ME!
############
#
# Everything that has been set in buildrc.sh can be
# overwritten here.
#
# Customise the repository location.
export REPOSITORY=${TANGO_CONTROLS_REPO_HOST}/hdbpp/${MY_NAME}.git

# Customise the branch which will be checked out by setting TANGO_BRANCH_OR_TAG.
export BRANCH_OR_TAG=${TANGO_BRANCH_OR_TAG}

# Tell libhdbpp-timescale to build shared libraries. hdbpp-es needs them.
ADDITIONAL_CMAKE_OPTIONS="-DBUILD_SHARED_LIBS=ON"

build_generic ${MY_NAME} ${REPOSITORY} ${BRANCH_OR_TAG} "${ADDITIONAL_CMAKE_OPTIONS}" || exit
end_message
)
