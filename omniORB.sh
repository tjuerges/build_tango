#! /usr/bin/env bash

# Copyright 2022, Thomas Juerges
# SPDX-License-Identifier: Apache-2.0

# omniORB build script for Linux distros without the matching package.

ABSOLUTE_PATH=$(realpath $(dirname ${BASH_SOURCE}))
export MY_PATH=${1:-$(realpath ${ABSOLUTE_PATH})}
SHORT_NAME=$(basename ${BASH_SOURCE})
export MY_NAME=${SHORT_NAME%.sh}

# Run everything in sub-shells. Saves me from all the pushd/popd.
(
# Load the default settings.
. ${MY_PATH}/settingsrc.sh

begin_message

# If omniORB is already installed, skip building and installing it.
[[ -f ${OMNIORB_INSTALLATION_DIR}/bin/catior ]] && { _p "It appears that omniORB is already installed in ${OMNIORB_INSTALLATION_DIR}. Will skip this step. If you want to re-install omniORB, then please \"rm -rf ${OMNIORB_INSTALLATION_DIR}\"."; exit 0; }

[[ ${OS} = Darwin && $(pkg-config --variable=prefix openssl | grep -c openssl) -eq 0 ]] && { _p "The OpenSSL package does not specify the prefix variable. If that variable is not set, omniORB will not compile libomnisslTK. But libomnisslTK is needed fpr TLS encryption. Exiting."; exit -1; }

create_top_level_build_dir || exit
[[ -d ${MY_NAME} ]] && rm -rf ${MY_NAME}
create_tool_build_dir || exit

OMNIORB_NAME=omniORB-${TANGO_BRANCH_OR_TAG}
OMNIORB_TARBALL=${OMNIORB_NAME}.tar.bz2

_p "Fetching and unpacking ${MY_NAME} ${TANGO_BRANCH_OR_TAG}..."
curl -L -o ${OMNIORB_TARBALL} https://sourceforge.net/projects/omniorb/files/omniORB/${OMNIORB_NAME}/${OMNIORB_TARBALL}/download || exit
tar -x --strip-components=1 -f ${OMNIORB_TARBALL} || exit

# Patch omniORB-4.3.2 to contain a correct libname
[[ ${TANGO_BRANCH_OR_TAG} = 4.3.2 ]] && patch -p1 < ${MY_PATH}/patch/omniORB-4.3.2.patch

mkdir build || exit
cd build || exit

# Enable TLS and disable unused longdoubles.
CONFIGURATION="--with-openssl --disable-longdouble"
# When building on Gentoo or inside Docker, the --build parameter should be provided.
[[ -f /etc/gentoo-release || ${DOCKER_ENV} -eq 1 ]] && CONFIGURATION="${CONFIGURATION} --build=$(uname -m)-unknown-linux-gnu"
[[ ${OS} = Darwin ]] && CONFIGURATION="${CONFIGURATION} --enable-cfnetwork"

_p "Configuring ${MY_NAME} ${TANGO_BRANCH_OR_TAG} with parameters ${CONFIGURATION} and installation PATH ${OMNIORB_INSTALLATION_DIR}..."
../configure --prefix=${OMNIORB_INSTALLATION_DIR} ${CONFIGURATION} || exit

_p "Building ${MY_NAME} ${TANGO_BRANCH_OR_TAG}..."
make -j${NCPU} all || exit

_p "Installing ${MY_NAME} ${TANGO_BRANCH_OR_TAG}..."
make -j${NCPU} install || exit

end_message
)
