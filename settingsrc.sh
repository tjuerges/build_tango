# Copyright 2022, Thomas Juerges
# SPDX-License-Identifier: Apache-2.0

# General settings for Tango build scripts for macOS.

function _p()
{
    echo -e "\n*****\tbuild_tango: ${*}"
}

function _e()
{
    if [ ${#} -eq 0 ]; then
        ret=${?}
    elif [ ${#} -eq 1 ]; then
        ret=${1}
    else
        _p "ERROR: No return status!"
        exit 42
    fi
    _p "RETURN ${ret}"
    [[ ${ret} -ne 0 ]] && exit ${ret}
}

# This is realpath for paths that do not exist on the file system.
# I am using it for normalising the omniORB installation path which
# might not exist yet.
function normpath()
{
    # Remove all /./ sequences.
    local path=${1//\/.\//\/}

    # Remove dir/.. sequences.
    while [[ ${path} =~ ([^/][^/]*/\.\./) ]]; do
        path=${path/${BASH_REMATCH[0]}/}
    done
    echo ${path}
}


# Check that his is Bash.
[[ ${SHELL##*/} != bash ]] && { _p "The build scripts must run in Bash. Please follow the instructions in README.md before continuing."; _e -1; }

# Check on which OS and CPU architecture we are: Is it Darwin (macOS) or Linux?
# If it is Darwin, then brew will have different installation locations.
OS=$(uname)
ARCH=$(uname -m)

[[ ${OS} = Darwin ]] && export _TANGO_MACOS_VERSION=$(sw_vers -productVersion | cut -d'.' -f1)

# Make certain that the realpath command is available. On Linux, realpath is
# part of the base system. On macOS it is available only from version 13 on.
# On macOS < 13 one needs to install the coreutils package.
# Begin by checking if this is macOS.
function realpath_error()
{
    echo -e "\n\n\t***** ERROR! This system (macOS ${_TANGO_MACOS_VERSION}) is supposed to have the realpath command available, but it does not. One cause could be that your PATH environment variable is not properly set. Please verify it. The build scripts cannot continue, because the realpath command is essential for their functioning. This script will now exit."
    _e -1
}

[[ -z ${_TANGO_DO_NOT_CHECK_FOR_COREUTILS_} ]] && [[ ${OS} = Darwin ]] && {
    # OK, it is macOS. Can realpath be found in the PATH?
    type realpath &> /dev/null
    [[ ${?} -eq 0 ]] || {
        # No.
        # Then check the macOS version.
        # If it is 13 or if the coreutils package is already installed, then
        # there is something seriously wrong and I cannot fix it here.
        # If the version is 12 or lower and the coreutils package has not been
        # installed yet, then I just install it.
        if [ ${_TANGO_MACOS_VERSION} -le 12 ]; then
            brew list coreutils &> /dev/null
            [[ ${?} -eq 0 ]] && realpath_error
            brew install coreutils
        else
            realpath_error
        fi
    }
    # Tell all build scripts to not check for the existence of the realpath
    # tool again.
    export _TANGO_DO_NOT_CHECK_FOR_COREUTILS_=1
}

# Check if this is a Gentoo Linux host. Gentoo is a bit different when it
# comes to installation paths for libraries. :-/
OS_LIB_DIR=lib
[[ ${OS} = Linux ]] && [[ -f /etc/gentoo-release ]] && {
    case ${ARCH} in
        aarch64|x86_64)
            OS_LIB_DIR=lib64
            ;;
    esac;
};

if [ ${OS} = Darwin ]; then
# Set the path for Homebrew. On my system I have ${BREW_PATH} set.
export BREW_DEFAULT_ROOT_DIR=${BREW_PATH}
# But others might not. Therefore check and set if still unset.
[[ -z ${BREW_DEFAULT_ROOT_DIR} ]] && export BREW_DEFAULT_ROOT_DIR=$(brew --prefix)


############
# MODIFY ME!
############
# Here you can set the path to brew's root directory.
# Usually you should leave this alone as it will just work if brew has been
# installed in the default location.
BREW_ROOT_DIR=${BREW_DEFAULT_ROOT_DIR}
fi

# Before anything is built, it will be checked if these packages
# are available on your system.
#
# It is essential that omniorb and zeromq are installed with brew!
#
# You can chose to remove git and cmake from this list if they have
# already been installed but not with brew.
export CHECK_AND_INSTALL_PACKAGES="git cmake pkg-config catch2 zeromq cppzmq omniorb libsodium boost-python3 jpeg-turbo opentelemetry-cpp"
if [ ${OS} != Darwin ]; then
    # I am assuming, that a Linux system is not using Linux brew.
    # Therefore keep the list empty and print a reminder.
    _p "Note: I hope that you have\n$(for p in ${CHECK_AND_INSTALL_PACKAGES}; do echo -e "\t${p}"; done;)\ninstalled. Otherwise this whole thing will not work."
    export CHECK_AND_INSTALL_PACKAGES=""
fi

# If set to 1, then cmake will be verbose.
[[ -z ${TANGO_CMAKE_BE_VERBOSE} ]] && export TANGO_CMAKE_BE_VERBOSE=0
TANGO_CMAKE_VERBOSE=""
[[ ${TANGO_CMAKE_BE_VERBOSE} -eq 1 ]] && TANGO_CMAKE_VERBOSE="--verbose"

# If set to 1, then the TangoDB will be built.
[[ -z ${TANGO_BUILD_TANGODB} ]] && export TANGO_BUILD_TANGODB=0

# If set to 1, then an existing TangoDB will replaced with a new DB.
[[ -z ${TANGO_REPLACE_TANGODB} ]] && export TANGO_REPLACE_TANGODB=0

# If set to 1, then tests will be built.
[[ -z ${TANGO_BUILD_TESTS} ]] && export TANGO_BUILD_TESTS=0
BUILD_TESTING="OFF"
[[ ${TANGO_BUILD_TESTS} -eq 1 ]] && BUILD_TESTING="ON"

# If set to 1, then tests will automatically be run.
[[ -z ${TANGO_RUN_TESTS} ]] && export TANGO_RUN_TESTS=0

# Read the versions.sh file. It contains suggestions for default branches or
# tags and also the TANGO_DEFAULT_VERSION which is used as part of the build
# directory.
. ${MY_PATH}/versions.sh

# The default branch or tag that will be checked out and used for the build.
# The defaults are read from the file versions.sh. The branch or tag can be
# manually set with TANGO_BRANCH_OR_TAG.
[[ -z ${TANGO_BRANCH_OR_TAG} ]] && [[ ! -z ${MY_PATH} ]] && {
    version_variable=${MY_NAME/-/_}_DEFAULT_VERSION
    version=${!version_variable}
    export TANGO_BRANCH_OR_TAG=${version}
}

# Use TANGO_DEFAULT_VERSION that is specified in the versions file as the
# general version, unless the user set it already.
[[ -z ${TANGO_VERSION} ]] && [[ ! -z ${TANGO_DEFAULT_VERSION} ]] && export TANGO_VERSION=${TANGO_DEFAULT_VERSION}

# This directory will be created and the entire build process happens
# there.
[[ -z ${TANGO_BUILD} ]] && export TANGO_BUILD=tango.${TANGO_VERSION}

# Build the packages without debug symbols and use the compiler
# optimisation. PyTango will be built with debug flags.
[[ -z ${TANGO_DEBUG_OR_RELEASE} ]] && export TANGO_DEBUG_OR_RELEASE=Release
export DEBUG_OR_RELEASE=${TANGO_DEBUG_OR_RELEASE}
[[ ${TANGO_DEBUG_OR_RELEASE} = Debug ]] && export DEBUG_OR_RELEASE=${TANGO_DEBUG_OR_RELEASE}

# When set, it is expected that this points to the local directory of the
# repository.
# The default is "empty" which tells the build scripts to git clone the repo.
# Setting it to an existing directory will instruct the build scripts to rsync
# a copy of the repo from the specified directory to the build directory.
[[ -z ${TANGO_REPO_LOCAL_DIR} ]] && export TANGO_CLONE_REPO="ON"

# Default repository location.
[[ -z ${TANGO_CONTROLS_REPO_HOST} ]] && TANGO_CONTROLS_REPO_HOST=https://gitlab.com/tango-controls


#
# IMPORTANT!
#
# This is the installation directory for everything which gets built here.
# You can conveniently overwrite this by exporting TANGO_INSTALLATION_DIR
# before you run any of the scripts in this repo.
[[ -z ${TANGO_INSTALLATION_DIR} ]] && export TANGO_INSTALLATION_DIR=${HOME}/.local/tango/tango.${TANGO_VERSION}
mkdir -p ${TANGO_INSTALLATION_DIR};
_p "The software will be installed to \"${TANGO_INSTALLATION_DIR}\"."

# On Linux omniORB is installed in its own directory. This allows to nuke the
# tango directory, but keep the omniORB installation.
[[ ${OS} = Darwin ]] && export OMNIORB_INSTALLATION_DIR=${BREW_ROOT_DIR}
[[ ${OS} = Linux ]] && export OMNIORB_INSTALLATION_DIR=$(normpath ${TANGO_INSTALLATION_DIR}/../omniORB.${omniORB_DEFAULT_VERSION})
[[ ! -z ${TANGO_OMNIORB_INSTALLATION_DIR} ]] && export OMNIORB_INSTALLATION_DIR=${TANGO_OMNIORB_INSTALLATION_DIR}
_p "Will install/use omniORB in directory \"${OMNIORB_INSTALLATION_DIR}\"."

# Set this to 1 if you do not want to build packages. This can be useful if
# you just want to run tests. In that case check also TANGO_NO_INSTALL!
[[ -z ${TANGO_NO_BUILD} ]] && export TANGO_NO_BUILD=0

# Set this to 1 if you do not want to install packages. This can be useful if
# you just want to check if a package builds or if your want to run tests.
[[ -z ${TANGO_NO_INSTALL} ]] && export TANGO_NO_INSTALL=0

# The name of the Boost-Python library which is needed for pyTango.
[[ -z ${TANGO_PYTANGO_BOOST_PYTHON_LIB} ]] && export TANGO_PYTANGO_BOOST_PYTHON_LIB=boost_python311

# Set the number of CPU cores you would like to use for the
# compilations.  The nproc command just returns the number of available
# cores.
if [ -z ${NCPU} ]; then
    if [ ${OS} = Darwin ]; then
        export NCPU=$(sysctl -n hw.logicalcpu)
    else
        export NCPU=$(nproc)
    fi
fi
export CMAKE_BUILD_PARALLEL_LEVEL=${NCPU}

# Set the language standards.
[[ -z ${CXX_STD} ]] && export CXX_STD=17
[[ -z ${C_STD} ]] && export C_STD=11

# Set the compiler.
if [ -z ${CC} ]; then
    if [ ${OS} = Darwin ]; then
        export CC=/usr/bin/clang
    else
        # It is likely that a Linux system uses gcc.
        export CC=gcc
    fi
fi

# Set the compiler.
if [ -z ${CXX} ]; then
    if [ ${OS} = Darwin ]; then
        export CXX=${CC}++
    else
        # It is likely that a Linux system uses gcc.
        export CXX=${CC//cc/++}
    fi
fi


# Generate debug symbols in the output?
# This is compiler dependent.
export DEBUG=""
[[ ${DEBUG_OR_RELEASE} = Debug ]] &&  { if [ "${CXX}" = "clang++" ]; then
    export DEBUG="-gmodules"
else
    export DEBUG="-ggdb3"
fi; }

# Set the compilation and linker flags.
#
export CFLAGS="-pipe ${CFLAGS}"
export CXXFLAGS="-pipe ${CXXFLAGS}"
# This tells the loader where to find omniORB and libtango.
OMNIORB_LDFLAGS=""
# Add the omniORB libray dir only if it was not installed with Homebrew, e.g.
# we are on Linux.
[[ ${BREW_ROOT_DIR} != ${OMNIORB_INSTALLATION_DIR} ]] && OMNIORB_LDFLAGS="-Wl,-rpath,${OMNIORB_INSTALLATION_DIR}/lib"
export LDFLAGS="${LDFLAGS} -Wl,-rpath,${TANGO_INSTALLATION_DIR}/${OS_LIB_DIR} ${OMNIORB_LDFLAGS}"

if [ ${OS} = Darwin ]; then
    # This compiler option and the exported environment variable
    # MACOSX_DEPLOYMENT_TARGET are now needed on macOS since Xcode 14.1. It
    # appears that Apple's clang++ 14.0.0 requires this to make compilations
    # with PCHs go through without errors. Without this parameter compilation
    # of cppTango will fail. The error message is about some compilation units
    # being compiled for a certain macOS target and other parts for a different
    # target version which is newer.
    # The additional environment variable MACOSX_DEPLOYMENT_TARGET is necessary
    # because, at this moment, the building of the pre-compiled headers fails
    # without it on macOS < 13.0.
    export MACOSX_DEPLOYMENT_TARGET=${_TANGO_MACOS_VERSION}.0
    export CXXFLAGS="${CXXFLAGS} -mmacosx-version-min=${MACOSX_DEPLOYMENT_TARGET} -I${BREW_ROOT_DIR}/include -I${TANGO_INSTALLATION_DIR}/include"
else
    # On non-macOS cppzmq is installed in ${TANGO_INSTALLATION_DIR}. cppzmq
    # does not have a proper pkg-config file, hence add the include dir.
    export CXXFLAGS="${CXXFLAGS} -I${TANGO_INSTALLATION_DIR}/include"
fi
# Add the omniORB header files dir only if it was not installed with Homebrew,
# e.g. we are on Linux.
[[ ${BREW_ROOT_DIR} != ${OMNIORB_INSTALLATION_DIR} ]] && export CXXFLAGS="${CXXFLAGS} -I${OMNIORB_INSTALLATION_DIR}/include"

# Now add the C amd CXX flags that the user specified.
[[ ! -z ${TANGO_CFLAGS} ]] && export CFLAGS="${CFLAGS} ${TANGO_CFLAGS}"
[[ ! -z ${TANGO_CXXFLAGS} ]] && export CXXFLAGS="${CXXFLAGS} ${TANGO_CXXFLAGS}"

if [ ${OS} = Darwin ]; then
    export PKG_CONFIG_PATH="${PKG_CONFIG_PATH}:${BREW_ROOT_DIR}/lib/pkgconfig"
else
    export PKG_CONFIG_PATH="${PKG_CONFIG_PATH}:/usr/${OS_LIB_DIR}/pkgconfig"
fi
# Add the omniORB and Tango installation dirs as pkg-config paths and prefer
# them over the already existing ones. This will find omniORB if it was
# compiled and installed from source.
export PKG_CONFIG_PATH="${TANGO_INSTALLATION_DIR}/lib/pkgconfig:${PKG_CONFIG_PATH}"
# On Gentoo 64 bit there are unfortunately two lib dirs: .../lib and .../lib64
[[ ${OS_LIB_DIR} = lib64 ]] && export PKG_CONFIG_PATH="${TANGO_INSTALLATION_DIR}/${OS_LIB_DIR}/pkgconfig:${PKG_CONFIG_PATH}"
# Only add the omniORB installation directory if it has not been installed
# with Homebrew, e.g. we are on Linux.
[[ ${BREW_ROOT_DIR} != ${OMNIORB_INSTALLATION_DIR} ]] && export PKG_CONFIG_PATH="${OMNIORB_INSTALLATION_DIR}/lib/pkgconfig:${PKG_CONFIG_PATH}"

# Set the default CMAKE options.
export DEFAULT_CMAKE_OPTIONS="-DCMAKE_CXX_COMPILER=${CXX} -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=ON -DCMAKE_INSTALL_PREFIX=${TANGO_INSTALLATION_DIR} -DCMAKE_BUILD_TYPE=${DEBUG_OR_RELEASE} -DBUILD_TESTING=${BUILD_TESTING}"
[[ ${BREW_ROOT_DIR} != ${OMNIORB_INSTALLATION_DIR} ]] && export DEFAULT_CMAKE_OPTIONS="${DEFAULT_CMAKE_OPTIONS} -DomniORB4_ROOT=${OMNIORB_INSTALLATION_DIR}"

# Allow the user to specify additional cmake defines.
[[ ! -z ${TANGO_ADDITIONAL_CMAKE_DEFINES} ]] && export DEFAULT_CMAKE_OPTIONS="${DEFAULT_CMAKE_OPTIONS} ${TANGO_ADDITIONAL_CMAKE_DEFINES}"


#
# Leave this alone.
#

# Make this host known as the TANGO_HOST unless already defined.
[[ -z ${TANGO_HOST} ]] && export TANGO_HOST=$(hostname -f):10000

function begin_message()
{
    PACKAGE_MSG="Building and installing ${MY_NAME}"
    [[ ${#} -eq 1 ]] && PACKAGE_MSG="${1}"
    _p "${PACKAGE_MSG}..."
}

function end_message()
{
    # Preserve the previous return code and return it from here again.
    previous_ret=${?}
    PACKAGE_MSG="Building and installing ${MY_NAME}"
    [[ ${#} -eq 1 ]] && PACKAGE_MSG="${1}"
    _p "${PACKAGE_MSG} done."
    return ${previous_ret}
}

function build_generic()
{
    [[ ${OS} = Darwin ]] && [[ ${ARCH} = aarch64 ]] && [[ -z ${TANGO_OMNITHREAD_IS_PATCHED} ]] && check_if_omniORB_is_broken_and_patch_it

    MY_NAME=${1}
    REPOSITORY=${2}
    BRANCH_OR_TAG=${3}
    CMAKE_OPTIONS=${4}
    create_top_level_build_dir
    rsync_or_clone ${MY_NAME} ${REPOSITORY} ${BRANCH_OR_TAG} ${TANGO_REPO_LOCAL_DIR} || { _p "Error. The rsync_or_clone function returned ${?}."; _e -1; }
    create_tool_build_dir
    _p "I will be using the following build flags:\n\tPKG_CONFIG_PATH=${PKG_CONFIG_PATH}\n\tPKG_LIBRARY_PATH=${PKG_LIBRARY_PATH}\n\tCXXFLAGS=${CXXFLAGS}\n\tLDFLAGS=${LDFLAGS}\n\tDEFAULT_CMAKE_OPTIONS=${DEFAULT_CMAKE_OPTIONS}\n\tCMAKE_OPTIONS=${CMAKE_OPTIONS}\n\tCMAKE_BUILD_PARALLEL_LEVEL=${CMAKE_BUILD_PARALLEL_LEVEL}"
    ret=0
    if [[ -z ${TANGO_NO_BUILD} ]] || [[ ${TANGO_NO_BUILD} -ne 1 ]]; then
        cmake ${DEFAULT_CMAKE_OPTIONS} ${CMAKE_OPTIONS} -S .. -B .
        ret=${?}
        if [ ${ret} -ne 0 ]; then
            exit ${ret}
        fi
        cmake --build . ${TANGO_CMAKE_VERBOSE}
        ret=${?}
        if [ ${ret} -ne 0 ]; then
            exit ${ret}
        fi
        if [[ -z ${TANGO_NO_INSTALL} ]] || [[ ${TANGO_NO_INSTALL} -ne 1 ]]; then
            cmake --install .
            ret=${?}
            if [ ${ret} -ne 0 ]; then
                exit ${ret}
            fi
        else
            _p "Skipping install because TANGO_NO_INSTALL=${TANGO_NO_INSTALL}."
        fi
    else
        _p "Skipping build and install because TANGO_NO_BUILD=${TANGO_NO_BUILD}."
    fi
    if [[ -z ${TANGO_RUN_TESTS} ]] || [[ ${TANGO_RUN_TESTS} -ne 1 ]]; then
        _p "Will skip running tests because TANGO_RUN_TESTS=${TANGO_RUN_TESTS}."
    else
        make test
        ret=${?}
        if [ ${ret} -ne 0 ]; then
            _e ${ret}
            exit ${ret}
        fi
    fi
}

function create_top_level_build_dir()
{
    [[ ! -d ${TANGO_BUILD} ]] && { mkdir -p build/${TANGO_BUILD} || _e; }
    cd build/${TANGO_BUILD}
    return 0
}

function create_tool_build_dir()
{
    [[ ! -d ${MY_NAME}/build ]] && { mkdir -p ${MY_NAME}/build || { _p "Error. Something is wrong with the build directory \"${MY_NAME}/build\"."; _p "PWD=${PWD}"; ls -l ${MY_NAME}; _e -1; }; }
    cd ${MY_NAME}/build
    return 0
}

function rsync_or_clone()
{
    LOCAL_REPO=${4}
    if [ ! -z "${LOCAL_REPO}" ]; then
        if [ ! -d "${LOCAL_REPO}" ]; then
            repo_name=$(basename ${LOCAL_REPO})
            repo_path=$(realpath ../../${LOCAL_REPO})
        else
            repo_path=$(realpath ${LOCAL_REPO})
        fi
        _p "Rsyncing local repo ${repo_name} with branch ${3} from ${repo_path}..."
        rsync --archive ${repo_path}/ ./${repo_name} || _e;
        cd ${repo_name} || _e;
        git checkout ${3} || _e;
        [[ -f build/cppapi/server/tango.h.gch ]] && { _p "Deleting pre-compiled header."; rm -rf build/cppapi/server/tango.h.gch; }
        cd ..
        _p "Rsyncing local repo ${repo_name} with branch ${3} from ${repo_path} done."
    else
        [[ -d ${1} ]] && rm -rf ${1}
        _p "Cloning branch ${3} from ${2}..."
        git clone --recurse-submodules --depth=1 --branch=${3} ${2}
        if [ ${?} -ne 0 ]; then
            _p "Failed to clone the branch ${3}. That's OK because I will now assume that it is a commit that I should check out and try again."
            git clone --recurse-submodules ${2}
            ret=${?}
            [[ ${ret} -ne 0 ]] && { _e "Cloning failed with return value=${ret}!"; return ${ret}; }
            git checkout ${3} && _p "Cloning branch ${3} from ${2} done."
        fi
    fi
    return 0
}

# Install a package with brew if it is not already installed
function brew_check_install()
{
    package=${1}
    [[ -z "$(brew list --versions ${package})" ]] && [[ -z "$(brew list --casks --versions ${package})" ]] && { brew install ${package} || _e; }
    _p "${package} is already installed.  Good."
    return 0
}
