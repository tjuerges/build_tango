#! /usr/bin/env bash

# Copyright 2022, Thomas Juerges
# SPDX-License-Identifier: Apache-2.0

# TangoDatabase build script for macOS.

ABSOLUTE_PATH=$(realpath $(dirname ${BASH_SOURCE}))
export MY_PATH=${1:-$(realpath ${ABSOLUTE_PATH})}
SHORT_NAME=$(basename ${BASH_SOURCE})
export MY_NAME=${SHORT_NAME%.sh}

# Run everything in sub-shells. Saves me from all the pushd/popd.
(
# Load the default settings.
. ${MY_PATH}/settingsrc.sh
begin_message

############
# MODIFY ME!
############

# Add MySQL client libs to PKG_PATH.
[[ ${OS} = Darwin ]] && export PKG_CONFIG_PATH=${PKG_CONFIG_PATH}:${BREW_ROOT_DIR}/opt/mariadb/lib/pkgconfig

# Note:
# At the moment (2022-08-30) there's something iffy with brew's mariadb package
# on macOS machines with M1/M2 cpus. Apparently the pkg-config file is not
# pointing to the correct mariadb/musql include directory.
[[ ${OS} = Darwin ]] && [[ ${ARCH} = arm64 ]] && [[ -z ${MYSQL_INCLUDE_DIR} ]] && export MYSQL_INCLUDE_DIR="${BREW_ROOT_DIR}/opt/mariadb/include/mysql"

#
# Everything that has been set in buildrc.sh can be
# overwritten here.
#
# Customise the repository location.
export REPOSITORY=${TANGO_CONTROLS_REPO_HOST}/${MY_NAME}.git

# Customise the branch which will be checked out by setting TANGO_BRANCH_OR_TAG.
export BRANCH_OR_TAG=${TANGO_BRANCH_OR_TAG}

begin_message "Installing required package mariadb..."
if [ ${OS} = Darwin ]; then
    brew_check_install mariadb
else
    _p "Note: I hope that you have\n\tmariadb\ninstalled. Otherwise this whole thing will not work."
fi
end_message "Installing required package mariadb done."

# See above. Conditionally add the MYSQL_INCLUDE_DIR def.
ADD_MYSQL_INCLUDE_DIR=""
[[ ! -z ${MYSQL_INCLUDE_DIR} ]] && { ADD_MYSQL_INCLUDE_DIR="-DMYSQL_INCLUDE_DIR=${MYSQL_INCLUDE_DIR}"; _p "Adding \"${ADD_MYSQL_INCLUDE_DIR}\" to the cmake parameters."; }
# Keep BUILD_TESTING set to OFF!
# Otherwise the tango db name will not be tango but tango_database_ci.
build_generic ${MY_NAME} ${REPOSITORY} ${BRANCH_OR_TAG} "-DMYSQL_ADMIN=tango_admin -DMYSQL_ADMIN_PASSWD=secret ${ADD_MYSQL_INCLUDE_DIR}" || exit
end_message
)
